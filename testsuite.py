import sys, os, unittest, pathsolver

# add tests to sys path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'src/test'))
print("Enhancing syspath with tests:", path)
sys.path.insert(0, path)

# discover all unit tests
test_suite = unittest.TestLoader().discover("src/test")

# execute and check if results were successful
test_results = unittest.TextTestRunner(verbosity=2).run(test_suite)

if not test_results.wasSuccessful():
    # test failed. Exit with code 1.
    sys.exit(1)