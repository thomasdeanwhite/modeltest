

class Loss():
    train_loss = 0
    valid_loss = 0
    test_loss = 0

    def __init__(self, train_loss=-1, valid_loss=-1, test_loss=-1):
        self.train_loss = train_loss
        self.valid_loss = valid_loss
        self.test_loss = test_loss


class Model():
    data = None
    last_loss = Loss()
    data_limit = -1

    def run(self, data):
        pass

    def train(self):
        return self.last_loss

    def evaluate(self):
        return self.last_loss

    def limit_data(self, limit):
        self.data_limit = limit

    def get_edges(self):
        return 0

    def get_nodes(self):
        return 0

    def get_layers(self):
        return 0

    def get_layer_shapes(self):
        return [0]

    def print_stats(self):
        print("Edges:", self.get_edges())
        print("Nodes:", self.get_nodes())
        print("Layers:", self.get_layers())
        layer_shapes = self.get_layer_shapes()
        print("\tInput:", layer_shapes[0], "Nodes.")
        for s in range(1, len(layer_shapes)-1):
            print("\t\tHidden Layer", s, ":", layer_shapes[s], "Nodes.")
        print("\tOutput:", layer_shapes[-1], "Nodes.")

