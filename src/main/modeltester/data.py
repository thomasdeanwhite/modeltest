from enum import Enum

class Dataset(Enum):
    TRAINING = 0
    VALIDATION = 1
    TESTING = 2


class DataProvider():
    def get_record(self, type, count):
        raise NotImplementedError()

    def count(self):
        raise NotImplementedError()


class Data ():
    datasets = None
    training_count = 0
    validation_count = 0
    testing_count = 0

    def __init__(self, data_generator, training_data=0.8, validation_data=0.1, testing_data=0.1):
        self.datasets = {}

        count = data_generator.count()

        self.training_count, self.validation_count, self.testing_count = int(training_data*count), \
                                            int(validation_data*count), int(testing_data*count)

        assert self.training_count + self.testing_count + self.validation_count <= count

        data_structure = {Dataset.TRAINING:self.training_count,
                          Dataset.VALIDATION:self.validation_count,
                          Dataset.TESTING:self.testing_count}

        records = 0

        for dataset in data_structure.keys():
            self.datasets[dataset] = [[], []]

            for i in range(data_structure[dataset]):
                x, y = data_generator.get_record(dataset, records)
                self.datasets[dataset][0].append(x)
                self.datasets[dataset][1].append(y)

                records += 1

    def get_dataset(self, dataset):
        return self.datasets[dataset]

