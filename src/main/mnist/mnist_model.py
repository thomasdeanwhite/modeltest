from modeltester.model import Model, Loss
from modeltester.data import DataProvider, Dataset, Data
import tensorflow as tf
import numpy as np


class MNistDataGen(DataProvider):
    data = []
    def __init__(self):
        mnist = tf.keras.datasets.mnist

        (train_x, train_y), (test_x, test_y) = mnist.load_data()

        x = np.append(train_x/255.0, test_x/255.0, axis=0)
        y = np.append(train_y, test_y, axis=0)

        self.data = [x, y]

    def get_record(self, type, count):
        return self.data[0][count], self.data[1][count]

    def count(self):
        return len(self.data[0])

class MNistModel(Model):

    model = None

    def __init__(self):

        self.data = Data(MNistDataGen())
        self.create_model()

    def create_model(self):
        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(28, 28)),
            tf.keras.layers.Dense(512, activation=tf.nn.relu),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(10, activation=tf.nn.softmax)
        ])
        self.model.compile(optimizer='adam',
                           loss='sparse_categorical_crossentropy',
                           metrics=['accuracy'])

    def run(self, data):
        return self.model.predict(data)

    def stack(self, x):
        return np.stack(x, axis=0)

    def train(self):
        train_data = self.data.get_dataset(Dataset.TRAINING)
        x = self.stack(train_data[0][:self.data_limit])
        y = self.stack(train_data[1][:self.data_limit])
        self.model.fit(x, y, epochs=40, verbose=0)

        train_loss = self.model.evaluate(self.stack(self.data.get_dataset(Dataset.TRAINING)[0]),
                                                    self.stack(self.data.get_dataset(Dataset.TRAINING)[1]), verbose=0)
        valid_loss = self.model.evaluate(self.stack(self.data.get_dataset(Dataset.VALIDATION)[0]),
                                                    self.stack(self.data.get_dataset(Dataset.VALIDATION)[1]), verbose=0)

        self.last_loss = Loss(train_loss=train_loss, valid_loss=valid_loss)

        return self.last_loss

    def evaluate(self):
        train_loss = self.model.evaluate(self.stack(self.data.get_dataset(Dataset.TRAINING)[0]),
                                         self.stack(self.data.get_dataset(Dataset.TRAINING)[1]), verbose=0)
        valid_loss = self.model.evaluate(self.stack(self.data.get_dataset(Dataset.VALIDATION)[0]),
                                         self.stack(self.data.get_dataset(Dataset.VALIDATION)[1]), verbose=0)
        test_loss = self.model.evaluate(self.stack(self.data.get_dataset(Dataset.TESTING)[0]),
                                                   self.stack(self.data.get_dataset(Dataset.TESTING)[1]), verbose=0)

        self.last_loss = Loss(train_loss=train_loss, valid_loss=valid_loss, test_loss=test_loss)

        return self.last_loss

    def get_edges(self):
        total_parameters = 0
        for variable in tf.trainable_variables():
            shape = variable.get_shape()
            variable_parameters = 1
            for dim in shape:
                variable_parameters *= dim.value
            total_parameters += variable_parameters
        return total_parameters

    def get_nodes(self):
        c = 0
        for s in self.get_layer_shapes():
            c += s
        return c

    def get_layers(self):
        return 3

    def get_layer_shapes(self):
        return [28*28, 512, 10]



