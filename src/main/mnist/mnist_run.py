import pathsolver
from mnist_model import MNistModel

import sys
sys.path.insert(0,'../')

mnist_model = MNistModel()
iterations = 10

log_training = False

print("Model data loaded. Training")

limits = [(x+1)*1000 for x in range(40)]

with open("training.csv", "w+") as f:
    f.write("datalim,iteration,dataset,stat,value\n")

for iteration in range(iterations):
    for x in limits:
        mnist_model.create_model()
        mnist_model.limit_data(x)
        for i in range(1):
            loss = mnist_model.train()
            if log_training:
                with open("training.csv", "a+") as f:
                    f.write(str(x) + "," + str(iteration) + ",training,loss," + str(loss.train_loss[0]) + "\n")
                    f.write(str(x) + "," + str(iteration) + ",training,accuracy," + str(loss.train_loss[1]) + "\n")
                    f.write(str(x) + "," + str(iteration) + ",validation,loss," + str(loss.valid_loss[0]) + "\n")
                    f.write(str(x) + "," + str(iteration) + ",validation,accuracy," + str(loss.valid_loss[1]) + "\n")
            print(loss.train_loss, ",", loss.valid_loss)

        loss = mnist_model.evaluate()
        with open("training.csv", "a") as f:
            f.write(str(x) + "," + str(iteration) + ",test,loss," + str(loss.test_loss[0]) + "\n")
            f.write(str(x) + "," + str(iteration) + ",test,accuracy," + str(loss.test_loss[1]) + "\n")

        print(loss.train_loss, ",", loss.valid_loss, loss.test_loss)