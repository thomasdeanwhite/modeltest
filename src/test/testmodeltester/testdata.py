from modeltester.data import Data, DataProvider, Dataset
import unittest

class MockDataProvider(DataProvider):
    def get_record(self, type, count):
        # return x, y. y is always 0 for mock
        return type, 0

    def count(self):
        return 10

class TestData(unittest.TestCase):

    def test_data_assignment(self):

        mdp = MockDataProvider()

        data = Data(mdp, training_data=0.8, validation_data=0.1, testing_data=0.1)

        self.assertEquals(8, len(data.get_dataset(Dataset.TRAINING)[0]))
        self.assertEquals(1, len(data.get_dataset(Dataset.VALIDATION)[0]))
        self.assertEquals(1, len(data.get_dataset(Dataset.TESTING)[0]))


    def test_data_assignment_values(self):

        mdp = MockDataProvider()

        data = Data(mdp, training_data=0.8, validation_data=0.1, testing_data=0.1)

        self.assertTrue(Dataset.TRAINING in data.get_dataset(Dataset.TRAINING)[0])
        self.assertTrue(Dataset.VALIDATION in data.get_dataset(Dataset.VALIDATION)[0])
        self.assertTrue(Dataset.TESTING in data.get_dataset(Dataset.TESTING)[0])

        self.assertFalse(Dataset.VALIDATION in data.get_dataset(Dataset.TRAINING)[0])
        self.assertFalse(Dataset.TESTING in data.get_dataset(Dataset.VALIDATION)[0])
        self.assertFalse(Dataset.TRAINING in data.get_dataset(Dataset.TESTING)[0])

        self.assertFalse(Dataset.TESTING in data.get_dataset(Dataset.TRAINING)[0])
        self.assertFalse(Dataset.TRAINING in data.get_dataset(Dataset.VALIDATION)[0])
        self.assertFalse(Dataset.VALIDATION in data.get_dataset(Dataset.TESTING)[0])



