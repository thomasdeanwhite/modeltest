import os, sys

# script to add src/main and src/test to python path and run test suite.
path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'src/main'))
print("Enhancing syspath with src:", path)
sys.path.insert(0, path)